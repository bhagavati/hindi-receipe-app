package com.hindi.recipe.app.Utility;

import com.hindi.recipe.app.Model.RecipesItemModel;

import java.util.ArrayList;

public class SingleToneData {

   public ArrayList<RecipesItemModel> ArReceipeDataList=new ArrayList<>();
        private static final SingleToneData ourInstance = new SingleToneData();

    public static SingleToneData getInstance() {

        return ourInstance;
    }

   public void SetArrayListdata(ArrayList<RecipesItemModel> ArReceipeDataList)
   {

      this.ArReceipeDataList.addAll(ArReceipeDataList);
   }

   public ArrayList<RecipesItemModel>GetArReceipeListData()
   {

      return  ArReceipeDataList;
   }



}