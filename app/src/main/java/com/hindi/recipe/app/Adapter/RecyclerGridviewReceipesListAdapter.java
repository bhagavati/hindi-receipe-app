package com.hindi.recipe.app.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.hindi.recipe.app.CallbackListener.RecycleviewitemClickListener;
import com.hindi.recipe.app.R;
import com.hindi.recipe.app.Utility.Utilis;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerGridviewReceipesListAdapter extends RecyclerView.Adapter<RecyclerGridviewReceipesListAdapter.MyViewHolder> {



    private Context context;
    private View itemView;
    private String drawableimage[];
    private String name[];
    private RecycleviewitemClickListener recycleviewitemClickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_recipes_icon)
        ImageView imgRecipesIcon;
        @BindView(R.id.txt_recipes_name)
        AppCompatTextView txtRecipesName;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public RecyclerGridviewReceipesListAdapter(Context context, String drawableimage[], String name[], RecycleviewitemClickListener recycleviewitemClickListener) {
        this.context = context;
        this.context = context;
        this.drawableimage = drawableimage;
        this.name = name;
        this.recycleviewitemClickListener = recycleviewitemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflator_recipe_list_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Glide.with(context).load(Utilis.getImageUsingName(context,drawableimage[position])).into(holder.imgRecipesIcon);


        holder.txtRecipesName.setText(name[position]);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recycleviewitemClickListener.OnRecycleviewItemClicked(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return drawableimage.length;
    }


}

