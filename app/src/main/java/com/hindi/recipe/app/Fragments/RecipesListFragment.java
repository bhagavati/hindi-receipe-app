package com.hindi.recipe.app.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.hindi.recipe.app.Activity.RecipeItemListActivity;
import com.hindi.recipe.app.Adapter.RecyclerGridviewReceipesListAdapter;
import com.hindi.recipe.app.CallbackListener.RecycleviewitemClickListener;
import com.hindi.recipe.app.R;
import com.hindi.recipe.app.Utility.ItemOffsetDecoration;
import com.hindi.recipe.app.Utility.Utilis;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class RecipesListFragment extends Fragment implements RecycleviewitemClickListener {

    Unbinder unbinder;

    @BindView(R.id.edt_Search_recipes_list)
    EditText edtSearchRecipesList;
    @BindView(R.id.banner_contain)
    RelativeLayout bannercontain;
    @BindView(R.id.gridview_Recipes_list)
    RecyclerView gridviewRecipesList;
    private View Convertview;

    private String drawableimagelist[];
    private String recipesnamelist[];


    private AdView bannerAdView;
    private InterstitialAd interstitialAd;
    private int Selected_position = 0;
    private RecyclerGridviewReceipesListAdapter recyclerGridviewReceipesListAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Convertview = inflater.inflate(R.layout.fragment_recipes_list, container, false);
        unbinder = ButterKnife.bind(this, Convertview);
        init();
        return Convertview;
    }


    private void init() {
        interstitialAdFullAds();
        BannerLoad();
        drawableimagelist = getContext().getResources().getStringArray(R.array.recipe_menu_icon_name);

        recipesnamelist = getContext().getResources().getStringArray(R.array.recipe_menu);



        gridviewRecipesList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getActivity(), R.dimen._5sdp);
        gridviewRecipesList.addItemDecoration(itemDecoration);

        recyclerGridviewReceipesListAdapter=new RecyclerGridviewReceipesListAdapter(getActivity(),drawableimagelist,recipesnamelist,this);
        gridviewRecipesList.setAdapter(recyclerGridviewReceipesListAdapter);
    }

    private void BannerLoad() {

        if (bannerAdView != null) {
            bannerAdView.destroy();
            bannerAdView = null;
        }

        bannerAdView = new AdView(RecipesListFragment.this.getActivity(), Utilis.BANNER_ADS, AdSize.BANNER_HEIGHT_50);
        bannercontain.addView(bannerAdView);
        bannerAdView.loadAd(bannerAdView.buildLoadAdConfig().withAdListener(adListener).build());

    }

    private void interstitialAdFullAds() {
        if (interstitialAd != null) {
            interstitialAd.destroy();
            interstitialAd = null;
        }

        interstitialAd =
                new InterstitialAd(RecipesListFragment.this.getActivity(), Utilis.INTERSTITIAL_ADS);


        InterstitialAd.InterstitialLoadAdConfig loadAdConfig =
                interstitialAd
                        .buildLoadAdConfig()
                        .withAdListener(interstitialAdListener)
                        .build();
        interstitialAd.loadAd(loadAdConfig);
    }


    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroyView() {


        bannercontain.removeView(bannerAdView);
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (bannerAdView != null) {
            bannerAdView.destroy();
            bannerAdView = null;
        }

        if (interstitialAd != null) {
            interstitialAd.destroy();
            interstitialAd = null;
        }

        super.onDestroy();
    }

    @Override
    public void OnRecycleviewItemClicked(int position) {
        Selected_position = position;
        if (interstitialAd == null
                || !interstitialAd.isAdLoaded()
                || interstitialAd.isAdInvalidated()) {

            GotoNextActivity(position);

        } else {

            interstitialAd.show();

        }


    }

    private void GotoNextActivity(int position) {
        Intent intent = new Intent(getContext(), RecipeItemListActivity.class);
        intent.putExtra("id", (position + 1) + "");
        intent.putExtra("name", recipesnamelist[position]);
        startActivity(intent);
    }


    AdListener adListener = new AdListener() {
        @Override
        public void onError(Ad ad, AdError adError) {

        }

        @Override
        public void onAdLoaded(Ad ad) {


        }

        @Override
        public void onAdClicked(Ad ad) {

        }


        @Override
        public void onLoggingImpression(Ad ad) {

        }
    };


    InterstitialAdListener interstitialAdListener = new InterstitialAdListener() {
        @Override
        public void onInterstitialDisplayed(Ad ad) {

        }

        @Override
        public void onInterstitialDismissed(Ad ad) {


            interstitialAd.destroy();
            interstitialAd = null;
            if (isAdded()) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        interstitialAdFullAds();
                        GotoNextActivity(Selected_position);
                    }
                }, 250);

            }

        }

        @Override
        public void onError(Ad ad, AdError adError) {

        }

        @Override
        public void onAdLoaded(Ad ad) {


        }

        @Override
        public void onAdClicked(Ad ad) {

        }

        @Override
        public void onLoggingImpression(Ad ad) {

        }
    };


}
