package com.hindi.recipe.app.Utility;

import android.content.Context;
import android.util.Log;

import net.lingala.zip4j.core.ZipFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class AssetDatabaseOpenHelper {

//    private static final String DB_NAME = "shayarinew.sqlite";
//    private static final String DB_FILE_NAME = "a.zip";
    private Context context;


    private static final String DATABASE_PATH = "/data/data/com.hindi.recipe.app/databases/";
    private static final String DATABASE_NAME="a.zip";





    private static final String DATABASE = "/data/data/com.hindi.recipe.app/databases/a.sqlite";
    public AssetDatabaseOpenHelper(Context context) {
        this.context = context;
    }

    public void openDatabase()
    {
        try {






                if (new File(DATABASE).exists() == false) {
                    CopyDataBaseFromAsset();
                    UnZipFile();
                }

        } catch (Exception e) {
            Log.e("Error in copy database", e.toString());
        }
    }




    private void CopyDataBaseFromAsset() throws IOException {
        InputStream in = context.getAssets().open("a.zip");

        String outputFileName = DATABASE_PATH + DATABASE_NAME;
        File databaseFile = new File("/data/data/"+context.getPackageName()+"/databases/");

        if (!databaseFile.exists()) {
            databaseFile.mkdir();
        }

        OutputStream out = new FileOutputStream(outputFileName);

        byte[] buffer = new byte[1024];
        int length;


        while ((length = in.read(buffer)) > 0) {
            out.write(buffer, 0, length);
        }

        out.flush();
        out.close();
        in.close();

    }

    private void UnZipFile()
    {
        try {
            File src = new File("/data/data/"+context.getPackageName()+"/databases/a.zip");

            if(!src.getParentFile().exists())
            {
                src.mkdir();
            }
            ZipFile zipFile = new ZipFile(src);
            if (zipFile.isEncrypted()) {
                zipFile.setPassword(Utilis.Getpassword());
            }
            String dest = "/data/data/"+context.getPackageName()+"/databases/";
            zipFile.extractAll(dest);


            String fi="/data/data/"+context.getPackageName()+"/databases/a.sqlite";
            if(new File(fi).exists())
            {
                src.delete();
            }
        } catch (Exception e) {
            Log.e("Data Don",e.toString());
        }
    }

}