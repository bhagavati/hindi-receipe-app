package com.hindi.recipe.app.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.hindi.recipe.app.Activity.RecipeDescriptionActivity;
import com.hindi.recipe.app.Adapter.ReceipsfavoriteItemlistAdapter;
import com.hindi.recipe.app.CallbackListener.RecycleviewitemClickListener;
import com.hindi.recipe.app.Model.RecipesItemModel;
import com.hindi.recipe.app.MyApplication;
import com.hindi.recipe.app.R;
import com.hindi.recipe.app.Utility.DatabaseHelper;
import com.hindi.recipe.app.Utility.SingleToneData;
import com.hindi.recipe.app.Utility.Utilis;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class FavoriteFragment extends Fragment implements RecycleviewitemClickListener {
    Unbinder unbinder;
    @BindView(R.id.recycleview_favorite_list)
    RecyclerView recycleviewFavoriteList;
    @BindView(R.id.txt_center_message)
    AppCompatTextView txtCenterMessage;
    @BindView(R.id.swipetorefreshllayout)
    SwipeRefreshLayout swipetorefreshllayout;
    @BindView(R.id.banner_contain)
    RelativeLayout bannerContain;

    private View Convertview;
    private LinearLayoutManager linearLayoutManager;

    private ReceipsfavoriteItemlistAdapter receipsfavoriteItemlistAdapter;
    private ArrayList<RecipesItemModel> ArRecipesItemModelList;
    private DatabaseHelper databaseHelper;

    private SingleToneData singleToneData;
    private AdView bannerAdView;

    private Context context;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Convertview = inflater.inflate(R.layout.fragment_favorite, container, false);
        unbinder = ButterKnife.bind(this, Convertview);
        init();
        return Convertview;
    }

    private void init() {
        context=Convertview.getContext();
        BannerLoad();

        singleToneData = SingleToneData.getInstance();
        singleToneData.ArReceipeDataList.clear();
        ArRecipesItemModelList = new ArrayList<>();
        databaseHelper = new DatabaseHelper(getActivity());


        linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recycleviewFavoriteList.setLayoutManager(linearLayoutManager);


        swipetorefreshllayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                GetDataUsingDbMethod();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        GetDataUsingDbMethod();
    }

    private void GetDataUsingDbMethod() {
        ArRecipesItemModelList.clear();
        singleToneData.ArReceipeDataList.clear();
        ArRecipesItemModelList = databaseHelper.RecipesMenuFavoriteItemName();
        singleToneData.SetArrayListdata(ArRecipesItemModelList);

        BindRecycleview();
    }


    private void BindRecycleview() {
        swipetorefreshllayout.setRefreshing(false);
        if (ArRecipesItemModelList.size() > 0) {
            recycleviewFavoriteList.setVisibility(View.VISIBLE);
            txtCenterMessage.setVisibility(View.GONE);
            receipsfavoriteItemlistAdapter = new ReceipsfavoriteItemlistAdapter(getContext(), ArRecipesItemModelList, this, this);
            recycleviewFavoriteList.setAdapter(receipsfavoriteItemlistAdapter);
        } else {
            recycleviewFavoriteList.setVisibility(View.GONE);
            txtCenterMessage.setVisibility(View.VISIBLE);
        }


    }


    public void RemoveFromFavorite(int position) {
        String typeid = ArRecipesItemModelList.get(position).getTypeid();
        String rowid = ArRecipesItemModelList.get(position).getId();

        databaseHelper.FavoriteAndUnfavorite(typeid, rowid, "0");


        singleToneData.ArReceipeDataList.remove(position);


        ArRecipesItemModelList.remove(position);
        receipsfavoriteItemlistAdapter.notifyDataSetChanged();

        if (ArRecipesItemModelList.size() == 0) {
            BindRecycleview();
        }
    }



    private void BannerLoad()
    {

        if (bannerAdView != null) {
            bannerAdView.destroy();
            bannerAdView = null;
        }

       bannerAdView= new AdView(context, Utilis.BANNER_ADS, AdSize.BANNER_HEIGHT_50);
        bannerContain.addView(bannerAdView);
        bannerAdView.loadAd();
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
        bannerContain.removeView(bannerAdView);
        unbinder.unbind();
    }



    @Override
    public void onDestroy() {
        if (bannerAdView != null) {
            bannerAdView.destroy();
            bannerAdView = null;
        }
        super.onDestroy();
    }
    @Override
    public void OnRecycleviewItemClicked(int position) {



        Intent intent = new Intent(getActivity(), RecipeDescriptionActivity.class);
        intent.putExtra("pos", position);

        startActivity(intent);
    }
}
