package com.hindi.recipe.app.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.hindi.recipe.app.R;
import com.hindi.recipe.app.Utility.Utilis;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class RecipesFragmentDetails extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.txt_Description)
    AppCompatTextView txtDescription;

    private View Convertview;

    private String itemdesc = "";



    public static Fragment getInstance(String itemdesc) {
        Bundle bundle = new Bundle();
        bundle.putString("item", itemdesc);
        RecipesFragmentDetails tabFragment = new RecipesFragmentDetails();
        tabFragment.setArguments(bundle);
        return tabFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Convertview = inflater.inflate(R.layout.fragment_recipes_fragment_details, container, false);
        unbinder = ButterKnife.bind(this, Convertview);
        itemdesc = getArguments().getString("item");

        init();

        return Convertview;
    }


    private void init() {
        txtDescription.setText(itemdesc);

    }


    @Override
    public void onDestroyView() {

        super.onDestroyView();

        unbinder.unbind();
    }
}
