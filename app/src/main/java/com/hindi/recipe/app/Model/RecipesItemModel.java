package com.hindi.recipe.app.Model;

import java.io.Serializable;

public class RecipesItemModel implements Serializable
{
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String name;
    private String id;

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    private String iconName;

    private String rit;

    public String getRit() {
        return rit;
    }

    public void setRit(String rit) {
        this.rit = rit;
    }

    public String getSramgri() {
        return sramgri;
    }

    public void setSramgri(String sramgri) {
        this.sramgri = sramgri;
    }

    private String sramgri;

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    private String favorite;

    public String getTypeid() {
        return Typeid;
    }

    public void setTypeid(String typeid) {
        Typeid = typeid;
    }

    private String Typeid;
}
