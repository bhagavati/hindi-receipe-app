package com.hindi.recipe.app.dialog;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDialog;
import androidx.appcompat.widget.AppCompatTextView;

import com.hindi.recipe.app.CallbackListener.MessageDialogListener;
import com.hindi.recipe.app.R;

public class MessageDialog extends AppCompatDialog implements View.OnClickListener
{

    private MessageDialogListener messageDialogListener;
    private Context context;
    private AppCompatTextView txtOkMessagedialog,txtCancelMessagedialog,txtrateusMessagedialog,txtMessage,txtTitle;



    public MessageDialog(Context context, MessageDialogListener messageDialogListener) {
        super(context);
        this.context=context;
        this.messageDialogListener=messageDialogListener;


    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        setCancelable(false);
        setContentView(R.layout.dialog_message);



        txtOkMessagedialog=findViewById(R.id.txt_okay_messageconfirm);
        txtCancelMessagedialog=findViewById(R.id.txt_cancel_messageconfirm);

        txtrateusMessagedialog=findViewById(R.id.txt_rateus_messageconfirm);
        txtrateusMessagedialog.setOnClickListener(this);

        txtOkMessagedialog.setOnClickListener(this);
        txtCancelMessagedialog.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.txt_okay_messageconfirm:
                messageDialogListener.OnOkMessageDialogButtonClicked();
                break;
            case R.id.txt_cancel_messageconfirm:
                messageDialogListener.OnNoMessageDialogButtonClicked();
                break;
            case R.id.txt_rateus_messageconfirm:
                messageDialogListener.OnRateusMessageDialogButtonClicked();
                break;
        }
    }
}
