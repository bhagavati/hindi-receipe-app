package com.hindi.recipe.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.facebook.ads.RewardedVideoAdListener;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.hindi.recipe.app.Fragments.RecipesFragmentDetails;
import com.hindi.recipe.app.Model.RecipesItemModel;
import com.hindi.recipe.app.R;
import com.hindi.recipe.app.Utility.DatabaseHelper;
import com.hindi.recipe.app.Utility.SingleToneData;
import com.hindi.recipe.app.Utility.Utilis;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RecipeDescriptionActivity extends AppCompatActivity {

    @BindView(R.id.img_recipes_item)
    ImageView imgRecipesItem;
    @BindView(R.id.collapsingToolbarLayout)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.img_back_header)
    ImageView imgBackHeader;
    @BindView(R.id.txt_title_header)
    AppCompatTextView txtTitleHeader;
    @BindView(R.id.img_favorite_header)
    ImageView imgFavoriteHeader;
    @BindView(R.id.img_share_header)
    ImageView imgShareHeader;
    @BindView(R.id.banner_contain)
    RelativeLayout bannerContain;


    private int pos = 0;
    private boolean isfavorite = false;

    private DatabaseHelper databaseHelper;
    private ArrayList<RecipesItemModel> ArRecipesList;

    private InterstitialAd interstitialAd;

    private boolean isbackclicked = false;
    private AdView bannerAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_description);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        BannerLoad();

        interstitialAdFullAds();
        databaseHelper = new DatabaseHelper(this);
        ArRecipesList = new ArrayList<>();
        pos = getIntent().getIntExtra("pos", 0);
        ArRecipesList = SingleToneData.getInstance().GetArReceipeListData();
        createViewPager();
        createTabIcons();
        ComponentListener();
        SetData();
    }
    private void BannerLoad()
    {

        if (bannerAdView != null) {
            bannerAdView.destroy();
            bannerAdView = null;
        }

        bannerAdView= new AdView(this, Utilis.BANNER_ADS, AdSize.BANNER_HEIGHT_50);
        bannerContain.addView(bannerAdView);
        bannerAdView.loadAd();
    }


    @Override
    public void onDestroy() {
        if (bannerAdView != null) {
            bannerAdView.destroy();
            bannerAdView = null;
        }
        if (interstitialAd != null) {
            interstitialAd.destroy();
            interstitialAd = null;
        }

        super.onDestroy();
    }

    private void SetData() {


        if (ArRecipesList != null && ArRecipesList.size() > 0) {


            String isfav = ArRecipesList.get(pos).getFavorite();

            if (isfav.equalsIgnoreCase("0")) {
                isfavorite = false;
                imgFavoriteHeader.setImageResource(R.drawable.tab_favorite_icon);
            } else {
                isfavorite = true;
                imgFavoriteHeader.setImageResource(R.drawable.tab_favorite_fill_icon);
            }

            txtTitleHeader.setText(ArRecipesList.get(pos).getName());
            Glide.with(this).load(Utilis.getImageUsingName(this, ArRecipesList.get(pos).getIconName())).into(imgRecipesItem);

        }

    }

    private void createViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewpager.setAdapter(adapter);
        tabs.setupWithViewPager(viewpager);
    }

    private void createTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.view_tab_text, null);
        tabOne.setText(getResources().getString(R.string.rit_name));
        tabOne.setTextColor(getResources().getColor(R.color.safron_color));
        tabs.getTabAt(1).setCustomView(tabOne);


        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.view_tab_text, null);
        tabTwo.setText(getResources().getString(R.string.use_of_name));
        tabTwo.setTextColor(getResources().getColor(R.color.safron_color));
        tabs.getTabAt(0).setCustomView(tabTwo);

    }


    private void ComponentListener() {
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                AppCompatTextView textView = (AppCompatTextView) view.findViewById(R.id.tab_text);
                textView.setTextColor(getResources().getColor(R.color.safron_color));

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                AppCompatTextView textView = (AppCompatTextView) view.findViewById(R.id.tab_text);
                textView.setTextColor(getResources().getColor(R.color.safron_color));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @OnClick({R.id.img_back_header, R.id.img_favorite_header, R.id.img_share_header})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back_header:
                backbuttonclicked();
                break;
            case R.id.img_favorite_header:
                FavoriteButtonClicked();
                if (interstitialAd == null
                        || !interstitialAd.isAdLoaded()
                        || interstitialAd.isAdInvalidated()) {


                } else {

                    interstitialAd.show();

                }
                break;
            case R.id.img_share_header:

                ShareItemButtonClicked();
                if (interstitialAd == null
                        || !interstitialAd.isAdLoaded()
                        || interstitialAd.isAdInvalidated()) {


                } else {

                    interstitialAd.show();

                }
                break;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return RecipesFragmentDetails.getInstance(ArRecipesList.get(pos).getSramgri());
            } else {
                return RecipesFragmentDetails.getInstance(ArRecipesList.get(pos).getRit());
            }
        }

        @Override
        public int getCount() {
            return 2;
        }


    }

    private void FavoriteButtonClicked() {
        if (isfavorite) {
            databaseHelper.FavoriteAndUnfavorite(ArRecipesList.get(pos).getTypeid(), ArRecipesList.get(pos).getId(), "0");
            isfavorite = false;
            imgFavoriteHeader.setImageResource(R.drawable.tab_favorite_icon);
        } else {
            databaseHelper.FavoriteAndUnfavorite(ArRecipesList.get(pos).getTypeid(), ArRecipesList.get(pos).getId(), "1");
            isfavorite = true;
            imgFavoriteHeader.setImageResource(R.drawable.tab_favorite_fill_icon);
        }

    }


    private void ShareItemButtonClicked() {
        StringBuffer stringBuffer = new StringBuffer();
        String name = ArRecipesList.get(pos).getName();
        String SamagreeTtile = getResources().getString(R.string.use_of_name);
        String Samagree = ArRecipesList.get(pos).getSramgri();
        String vidhititle = getResources().getString(R.string.rit_name);
        String vidhi = ArRecipesList.get(pos).getRit();
        String link = "https://play.google.com/store/apps/details?id=" + getPackageName();
        stringBuffer.append(name);
        stringBuffer.append("\n\n");
        stringBuffer.append(SamagreeTtile);
        stringBuffer.append("\n");
        stringBuffer.append(Samagree);
        stringBuffer.append("\n\n");
        stringBuffer.append(vidhititle);
        stringBuffer.append("\n");
        stringBuffer.append(vidhi);
        stringBuffer.append("\n\n");
        stringBuffer.append("Download App \n" + link);


        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, stringBuffer.toString());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);

    }

    RewardedVideoAdListener rewardedVideoAdListener = new RewardedVideoAdListener() {

        @Override
        public void onRewardedVideoCompleted() {

        }

        @Override
        public void onError(Ad ad, AdError adError) {

            Log.e("Erorr", adError.getErrorMessage());
        }

        @Override
        public void onAdLoaded(Ad ad) {

        }

        @Override
        public void onAdClicked(Ad ad) {

        }

        @Override
        public void onLoggingImpression(Ad ad) {

        }

        @Override
        public void onRewardedVideoClosed() {

        }
    };


    private void interstitialAdFullAds() {
        if (interstitialAd != null) {
            interstitialAd.destroy();
            interstitialAd = null;
        }

        interstitialAd =
                new InterstitialAd(RecipeDescriptionActivity.this, Utilis.INTERSTITIAL_ADS);


        InterstitialAd.InterstitialLoadAdConfig loadAdConfig =
                interstitialAd
                        .buildLoadAdConfig()
                        .withAdListener(interstitialAdListener)
                        .build();
        interstitialAd.loadAd(loadAdConfig);
    }


    InterstitialAdListener interstitialAdListener = new InterstitialAdListener() {
        @Override
        public void onInterstitialDisplayed(Ad ad) {

        }

        @Override
        public void onInterstitialDismissed(Ad ad) {


            interstitialAd.destroy();
            interstitialAd = null;


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isbackclicked == true) {
                        onBackPressed();
                    } else {
                        interstitialAdFullAds();
                    }
                }
            }, 250);


        }

        @Override
        public void onError(Ad ad, AdError adError) {

        }

        @Override
        public void onAdLoaded(Ad ad) {


        }

        @Override
        public void onAdClicked(Ad ad) {

        }

        @Override
        public void onLoggingImpression(Ad ad) {

        }
    };

    @Override
    public void onBackPressed() {
        backbuttonclicked();
    }

    private void backbuttonclicked() {
        isbackclicked = true;
        if (interstitialAd == null
                || !interstitialAd.isAdLoaded()
                || interstitialAd.isAdInvalidated()) {

            finish();

        } else {

            interstitialAd.show();

        }
    }
}
