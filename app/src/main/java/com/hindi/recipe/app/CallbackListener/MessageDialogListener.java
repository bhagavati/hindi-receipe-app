package com.hindi.recipe.app.CallbackListener;

public interface MessageDialogListener {
    public void OnOkMessageDialogButtonClicked();
    public void OnNoMessageDialogButtonClicked();
    public void OnRateusMessageDialogButtonClicked();
}
