package com.hindi.recipe.app;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.facebook.ads.AudienceNetworkAds;
import com.hindi.recipe.app.Utility.AssetDatabaseOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MyApplication extends Application {
    private static MyApplication instance;
    @Override
    public void onCreate() {
        super.onCreate();
        AudienceNetworkAds.initialize(this);
        AssetDatabaseOpenHelper assetDatabaseOpenHelper=new AssetDatabaseOpenHelper(this);
        assetDatabaseOpenHelper.openDatabase();
    }








}
