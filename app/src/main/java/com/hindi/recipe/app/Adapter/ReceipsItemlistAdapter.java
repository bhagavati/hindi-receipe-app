package com.hindi.recipe.app.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdsManager;
import com.hindi.recipe.app.Activity.RecipeItemListActivity;
import com.hindi.recipe.app.CallbackListener.RecycleviewitemClickListener;
import com.hindi.recipe.app.Model.RecipesItemModel;
import com.hindi.recipe.app.R;
import com.hindi.recipe.app.Utility.Utilis;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReceipsItemlistAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {




    private RecycleviewitemClickListener recycleviewitemClickListener;

    private Context context;



    private ArrayList<RecipesItemModel> ArRecipesItemModelList;
    private static final int POST_TYPE = 0;
    private static final int AD_TYPE = 1;


    public ReceipsItemlistAdapter(Context context, ArrayList<RecipesItemModel> ArRecipesItemModelList, RecycleviewitemClickListener recycleviewitemClickListener) {
        this.context = context;


        this.ArRecipesItemModelList = ArRecipesItemModelList;
        this.recycleviewitemClickListener = recycleviewitemClickListener;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_recipes_icon)
        ImageView imgRecipesIcon;
        @BindView(R.id.txt_recipes_name)
        AppCompatTextView txtRecipesName;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {




            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflator_recipe_item_list_view, parent, false);

            return new MyViewHolder(itemView);



    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {



            MyViewHolder postHolder = (MyViewHolder) holder;


            MyViewHolderBindData(postHolder, position);


    }

    private void MyViewHolderBindData(MyViewHolder holder, int position) {
        holder.txtRecipesName.setText(ArRecipesItemModelList.get(position).getName());


        Glide.with(context).load(Utilis.getImageUsingName(context, ArRecipesItemModelList.get(position).getIconName())).into(holder.imgRecipesIcon);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recycleviewitemClickListener.OnRecycleviewItemClicked(position);
            }
        });
    }


    @Override
    public int getItemCount() {

        return ArRecipesItemModelList.size();
    }


    @Override
    public int getItemViewType(int position) {
        return  POST_TYPE;
    }




}