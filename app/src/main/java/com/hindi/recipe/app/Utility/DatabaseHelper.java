package com.hindi.recipe.app.Utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.hindi.recipe.app.Model.RecipesItemModel;

import java.io.File;
import java.util.ArrayList;

public class DatabaseHelper {



    private static final String DB_NAME = "a.sqlite";

    String iconName[]=new String[]{"a","b","c","d","e","f","g","h","i","j","l","m","k","o","p","q","r","w","s","v"};


    private Context context;

    private File dbFile;

    public DatabaseHelper(Context context) {
        this.context = context;
        dbFile = this.context.getDatabasePath(DB_NAME);

    }



    public ArrayList<RecipesItemModel> RecipesMenuItemName(String typeid) {

        SQLiteDatabase db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);
        ArrayList<RecipesItemModel> ArRecipesMenuItemList = new ArrayList<>();

        String selectQuery = "SELECT name,row_id,sahitya,kruti,fav FROM recipe where type_id="+ typeid +"";


        int pos=Integer.parseInt(typeid);
        int counter=1;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                RecipesItemModel recipesItemModel = new RecipesItemModel();
                recipesItemModel.setIconName(iconName[pos-1]+""+counter);

                recipesItemModel.setName(cursor.getString(0));
                recipesItemModel.setId(cursor.getString(1));
                recipesItemModel.setRit(cursor.getString(3));
                recipesItemModel.setSramgri(cursor.getString(2));
                recipesItemModel.setFavorite(cursor.getString(4));
                recipesItemModel.setTypeid(typeid);

                ArRecipesMenuItemList.add(recipesItemModel);
                counter=counter+1;
            } while (cursor.moveToNext());
        }


        db.close();


        return ArRecipesMenuItemList;
    }
    public ArrayList<RecipesItemModel> RecipesMenuFavoriteItemName() {

        SQLiteDatabase db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);
        ArrayList<RecipesItemModel> ArRecipesMenuItemList = new ArrayList<>();

        String selectQuery = "SELECT name,row_id,sahitya,kruti,fav,type_id FROM recipe where fav=1";




        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                RecipesItemModel recipesItemModel = new RecipesItemModel();

                String typeid=cursor.getString(5);
                String rowid=cursor.getString(1);

                int pos=Integer.parseInt(typeid);

                 recipesItemModel.setIconName(iconName[pos-1]+""+rowid);

                recipesItemModel.setName(cursor.getString(0));
                recipesItemModel.setId(rowid);
                recipesItemModel.setSramgri(cursor.getString(2));
                recipesItemModel.setRit(cursor.getString(3));
                recipesItemModel.setFavorite(cursor.getString(4));
                recipesItemModel.setTypeid(typeid);



                ArRecipesMenuItemList.add(recipesItemModel);

            } while (cursor.moveToNext());
        }


        db.close();


        return ArRecipesMenuItemList;
    }


    public  int FavoriteAndUnfavorite(String typeid,String row_id,String favorite)
    {

        SQLiteDatabase db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

        ContentValues cv = new ContentValues();
        cv.put("fav",favorite);
        int id=db.update("recipe", cv, "type_id="+typeid+" and row_id="+row_id, null);
        db.close();
        return id;
    }
}