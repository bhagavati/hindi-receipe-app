package com.hindi.recipe.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.hindi.recipe.app.Adapter.ReceipsItemlistAdapter;
import com.hindi.recipe.app.CallbackListener.RecycleviewitemClickListener;
import com.hindi.recipe.app.Model.RecipesItemModel;
import com.hindi.recipe.app.R;
import com.hindi.recipe.app.Utility.DatabaseHelper;
import com.hindi.recipe.app.Utility.SingleToneData;
import com.hindi.recipe.app.Utility.Utilis;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RecipeItemListActivity extends AppCompatActivity implements RecycleviewitemClickListener {

    @BindView(R.id.recycleview_list)
    RecyclerView recycleviewList;
    @BindView(R.id.img_back_header)
    ImageView imgBackHeader;
    @BindView(R.id.txt_title_header)
    AppCompatTextView txtTitleHeader;
    @BindView(R.id.banner_contain)
    RelativeLayout bannerContain;


    private LinearLayoutManager linearLayoutManager;
    private ReceipsItemlistAdapter receipsItemlistAdapter;

    private String Id = "";
    private DatabaseHelper databaseHelper;

    SingleToneData singleToneData;
    private String titlename = "";

    private ArrayList<RecipesItemModel> ArRecipesItemModelList;
    private AdView bannerAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_item_list);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        BannerLoad();

        singleToneData = SingleToneData.getInstance();
        Id = getIntent().getStringExtra("id");
        titlename = getIntent().getStringExtra("name");

        txtTitleHeader.setText(titlename);


        ArRecipesItemModelList = new ArrayList<>();
        databaseHelper = new DatabaseHelper(this);


        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recycleviewList.setLayoutManager(linearLayoutManager);


        GetDataUsingDbMethod();

    }

    private void BannerLoad()
    {

        if (bannerAdView != null) {
            bannerAdView.destroy();
            bannerAdView = null;
        }

        bannerAdView= new AdView(this, Utilis.BANNER_ADS, AdSize.BANNER_HEIGHT_50);
        bannerContain.addView(bannerAdView);
        bannerAdView.loadAd();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void OnRecycleviewItemClicked(int position) {


        Intent intent = new Intent(this, RecipeDescriptionActivity.class);
        intent.putExtra("pos", position);


        startActivity(intent);
    }

    private void GetDataUsingDbMethod() {
        ArRecipesItemModelList.clear();
        ArRecipesItemModelList = databaseHelper.RecipesMenuItemName(Id);

        if (ArRecipesItemModelList.size() > 0) {
            singleToneData.ArReceipeDataList.clear();
            singleToneData.SetArrayListdata(ArRecipesItemModelList);
            receipsItemlistAdapter = new ReceipsItemlistAdapter(this, ArRecipesItemModelList, this);
            recycleviewList.setAdapter(receipsItemlistAdapter);
        }

    }


    @Override
    public void onDestroy() {

        super.onDestroy();
        if (bannerAdView != null) {
            bannerAdView.destroy();
            bannerAdView = null;
        }
    }


    @OnClick({R.id.img_back_header})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back_header:
                onBackPressed();
                break;

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
