package com.hindi.recipe.app.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.hindi.recipe.app.CallbackListener.RecycleviewitemClickListener;
import com.hindi.recipe.app.Fragments.FavoriteFragment;
import com.hindi.recipe.app.Model.RecipesItemModel;
import com.hindi.recipe.app.R;
import com.hindi.recipe.app.Utility.Utilis;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReceipsfavoriteItemlistAdapter extends RecyclerView.Adapter<ReceipsfavoriteItemlistAdapter.MyViewHolder> {



    private RecycleviewitemClickListener recycleviewitemClickListener;

    private Context context;
    private View itemView;
    private ArrayList<RecipesItemModel> ArRecipesItemModelList;
    private FavoriteFragment favoriteFragment;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_recipes_icon)
        ImageView imgRecipesIcon;
        @BindView(R.id.txt_recipes_name)
        AppCompatTextView txtRecipesName;
        @BindView(R.id.img_favorite_header)
        ImageView imgFavoriteHeader;
        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public ReceipsfavoriteItemlistAdapter(Context context, ArrayList<RecipesItemModel> ArRecipesItemModelList, RecycleviewitemClickListener recycleviewitemClickListener, FavoriteFragment favoriteFragment) {
        this.context = context;
        this.recycleviewitemClickListener = recycleviewitemClickListener;
        this.ArRecipesItemModelList = ArRecipesItemModelList;
        this.favoriteFragment = favoriteFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflator_recipe_favorite_item_list_view, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.txtRecipesName.setText(ArRecipesItemModelList.get(position).getName());
        Glide.with(context).load(Utilis.getImageUsingName(context, ArRecipesItemModelList.get(position).getIconName())).into(holder.imgRecipesIcon);


        holder.imgFavoriteHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoriteFragment.RemoveFromFavorite(position);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recycleviewitemClickListener.OnRecycleviewItemClicked(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return ArRecipesItemModelList.size();
    }


}