package com.hindi.recipe.app.Activity;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.hindi.recipe.app.CallbackListener.MessageDialogListener;
import com.hindi.recipe.app.Fragments.FavoriteFragment;
import com.hindi.recipe.app.Fragments.RecipesListFragment;
import com.hindi.recipe.app.R;
import com.hindi.recipe.app.dialog.MessageDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MessageDialogListener{

    @BindView(R.id.frame_container)
    LinearLayout frameContainer;
    @BindView(R.id.bottom_nav)
    BottomNavigationView bottomNav;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private MessageDialog messageDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
    }


    private void init() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        getSupportActionBar().setTitle(getResources().getString(R.string.title_name));
        messageDialog=new MessageDialog(this,this);
        RecipesListFragment();
        ComponenetListener();
    }

    private void ComponenetListener() {
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.tab_recipes_list) {
                    RecipesListFragment();

                    return true;
                } else if (item.getItemId() == R.id.tab_recipes_favorite_list) {

                    RecipesFavoriteFragment();
                    return true;
                }
                return false;
            }
        });
    }


    private void RecipesListFragment() {
        RecipesListFragment settingFragment = new RecipesListFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_container, settingFragment);
        ft.commit();
    }


    private void RecipesFavoriteFragment() {

        FavoriteFragment favoriteFragment = new FavoriteFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_container, favoriteFragment);
        ft.commit();
    }



    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        if (menu instanceof MenuBuilder) {
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.share_app_option_menu:
                OnShareAppButtonClicked();
                return true;
            case R.id.privacy_app_option_menu:
                PrivacyPolicyButtonClicked();
                return true;

            case R.id.rate_us_option_menu:

                showRateAppFallbackDialog();
                return true;

            case R.id.more_app_option_menu:
                OnMoreappButtonClicked();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }







    private void showRateAppFallbackDialog() {
        new MaterialAlertDialogBuilder(this)
                .setTitle(R.string.rate_app_title)
                .setMessage(R.string.rate_app_message)
                .setPositiveButton(R.string.rate_btn_pos, (dialog, which) -> {

                    Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName()));
                    rateIntent.setPackage("com.android.vending");

                    startActivity(rateIntent);
                })

                .setNeutralButton(R.string.rate_btn_nut,
                        (dialog, which) -> {
                    dialog.dismiss();
                        })
                .setOnDismissListener(dialog -> {

                })
                .show();
    }

    private void ExitButtonClicked()
    {
            messageDialog.show();
    }

    @Override
    public void onBackPressed() {
        ExitButtonClicked();
    }

    @Override
    public void OnOkMessageDialogButtonClicked() {

        messageDialog.dismiss();
        finishAffinity();
    }

    @Override
    public void OnNoMessageDialogButtonClicked() {

        messageDialog.dismiss();
    }

    @Override
    public void OnRateusMessageDialogButtonClicked() {
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName()));
        rateIntent.setPackage("com.android.vending");
        startActivity(rateIntent);

    }



    private void OnShareAppButtonClicked()
    {
        try
        {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");

            String texdata="आप रोजाना नई रेसिपी बनाना चाहते हैं";
            String link = texdata+"\t\t Download this app \n https://play.google.com/store/apps/details?id=" + getPackageName();
            String shareBody = "\n" + link;

            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share App");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Please Install Share app", Toast.LENGTH_SHORT).show();
        }
    }

    private void OnMoreappButtonClicked() {
        String developerName = "Bhagavati infotech";
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=" + developerName)).setPackage("com.android.vending"));
        } catch (ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=" + developerName)));
        }
    }

    private void PrivacyPolicyButtonClicked() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://bhavrajapps.in/privacy/hindi_recipes/index.php")));
    }

}
